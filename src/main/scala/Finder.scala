
trait Finder {
  def findMissing(arr: Array[Long]): Array[Long]
}

object FinderImpl extends Finder {

  override def findMissing(arr: Array[Long]): Array[Long] = {
    if (arr.nonEmpty) {
      val set = arr.toSet
      val completeSet = (arr.min to arr.max).toSet
      completeSet.diff(set).toArray
    } else {
      Array()
    }
  }
}
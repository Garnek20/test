
trait Counter {
  def countDistinct(arr: Array[Long]): Long
}

object CounterImpl extends Counter{

  override def countDistinct(arr: Array[Long]): Long = {
    arr.toList.distinct.size
  }
}

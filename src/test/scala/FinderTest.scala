import org.scalatest.WordSpec

class FinderTest extends WordSpec {

  private val finder: Array[Long] => Array[Long] = FinderImpl.findMissing

  "A finder" when {
    "receives array [8 1 6 3]" should {
      "return [7 2 5 4]" in {
        val arrayIn: Array[Long] = Array(8, 1, 6, 3)
        val missingElements: Array[Long] = Array(7, 2, 5, 4)

        assert(isCorrect(missingElements, arrayIn))
      }
    }

    "receives an array of elements" should {
      "return missing elements" in {
        val arrayIn2 = (0L to(100000L, 2L)).toArray
        val missingElements2 = (1L to(100000L, 2L)).toArray

        assert(isCorrect(missingElements2, arrayIn2))
      }
    }

    "receives an array without missing elements" should {
      "return empty array" in {
        val arrayIn = (0L to(100L, 1L)).toArray

        assert(isCorrect(Array(), arrayIn))
      }
    }

    "receives an empty array" should {
      "return empty array" in {
        assert(isCorrect(Array(), Array()))
      }
    }
  }

  private def isCorrect(missingElements: Array[Long], inputArray: Array[Long]): Boolean = {
    missingElements
      .diff(finder(inputArray))
      .isEmpty
  }

}

import org.scalatest.WordSpec

class CounterTest extends WordSpec {

  private val counter: (Array[Long]) => Long = CounterImpl.countDistinct

  "A counter" should {
    "return count of distinct elements in array" in {
      val array: Array[Long] = Array(1, 1, 1, 1, 1, 1)
      assert(counter(array) == 1)

      val array2: Array[Long] = Array(1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4)
      assert(counter(array2) == 4)

      val array3: Array[Long] = ((1L to 100000L).toList ::: (1L to 1000L).toList ::: (1L to 1000L).toList).toArray
      assert(counter(array3) == 100000)
    }
  }
}
